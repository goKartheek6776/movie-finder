import React from 'react';
import './style.css';

class MovieInfo extends React.Component {
    
    render(){
        let { data } = this.props;
        return(
            <div>
                <h1>{data.label}</h1>
                <img className="poster" src={data.poster} alt="poster_image"/>
                <div className="badges">
                    <div className="imdb"></div>
                    <div className="imdb"></div>
                </div>
            </div>
        );
    }
}

export default MovieInfo;
