import React from 'react';
import Select from 'react-select';
// import {connect} from 'react-redux';
import MovieInfo from './MovieInfo/index.js';
// import { addCounter } from './actions.js';
import './App.css';

class App extends React.Component {
  constructor(props){
    super(props);
    
    this.state = {
      selectedOption: {
        label: 'Search movies..'
      },
      options: [],
      isMovieSelected: false,
      counter: 0,
    }
    
    this.fetchMovies = this.fetchMovies.bind(this);
    this.watchSelection = this.watchSelection.bind(this);
  }
  
  componentWillReceiveProps(nextProps) {
    console.log('np: ',nextProps);
    if(this.state.counter !== nextProps.score.counter){
      this.setState({
        counter : nextProps.score.counter
      })
    }
  }
  
  
  
  fetchMovies = (selectedOption) => {
    if(selectedOption.length >=3){
      const apikey = 'e260c91c';
      const url = 'https://www.omdbapi.com/?s=' + selectedOption + '&apikey=' + apikey;
      var self = this;
      fetch(url)
      .then((resp) => resp.json()) 
      .then(function(data) {
        if(data.Response === "True") {
          let options = data && data.Search.map(function (movie) {
            return { value: movie.Title, label: movie.Title, poster: movie.Poster, year: movie.Year };
          })
          self.setState({options})
        }
      })
    } 
  }
  
  watchSelection = (e) => {
    this.setState({
      selectedOption: e,
      isMovieSelected: true
    })
  }
  
  render() {
    const { selectedOption, isMovieSelected, options} = this.state;
    console.log(this.props)
    return (
      <div className="movie-finder">
      <Select
      value={selectedOption}
      onInputChange={this.fetchMovies}
      onChange={this.watchSelection}
      options={options}
      labelKey='Title'
      valueKey='Title'
      />
      {/* <button onClick={this.props.addCounter}>ADD</button> */}
      {
        isMovieSelected &&
        <MovieInfo data={selectedOption}/>
      }
      </div>
      
    );
  }
}

export default App;

// const mapStateToProps = score => {
//   return { score };
// };

// export default connect(mapStateToProps, {
//   addCounter
// })(App);
