const addCounter = (state = {counter : 0}, action) => {
    console.log("action in reducer", action);
    switch(action.type) {
      case "INCREMENT_COUNTER":
        return {...state, ...{counter: ++state.counter}};
      
      default:
        return state;
    }
   };
   
   export default addCounter;